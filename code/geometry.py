"""
Sub module for handling the geometry of an hexagonal bundle. The 
:class:`bundleBuilder.geometry.Circle` is used to build the rod cross sections, inside the 
hexagonal lattice, and to compute their discretization. The 
:class:`bundleBuilder.geometry.hexaBundleGeometry` computes all the geometrical information 
of the bundle, concerning dimensions, pin and subchannel numbering, connectivity 
of the subchannels.
"""

import numpy as np
import yaml
import copy 

class Circle:
    """
    Class used to build the rod cross sections.
    
    Attributes
    ----------
    centre : numpy.array
        array contaning the coordinates of the circle centre
    radius : float
        radius of the rod
    ID : int    
        rod ID

    Parameters
    ----------
    centre : list of floats
        List containing the coordinates of the pin centre
    radius : float 
        Pin radius 
    circID : int 
        Identifier of the pin        
    """    
    
    def __init__(self, centre, radius, circID):
        """
        Initialization function 
        
        Parameters
        ----------
        centre : list of floats
            List containing the coordinates of the pin centre
        radius : float 
            Pin radius 
        circID : int 
            Identifier of the pin
            
        """
        self.centre = np.array(centre, dtype=np.float)
        self.radius = radius
        self.ID     = circID
        
    def trPoint(self, angle, vector):
        """
        Function used to translate a point of the circle, from an input angle,
        using an input vector 

        Parameters
        ----------
        angle : float 
            The input angle, in radians 
        vector : numpy.array 
            The input numpy array for the translation vector 

        Returns
        -------
        newPoint : numpy.array 
            The translated point
        """

        newPoint = self.point(angle)
        return newPoint + vector

    def calcPoint(self, angle, radius):
        """
        Function used to calculate a point coordinates, with a distance equal
        to the input radius, from the circle centre, and with an angle equal 
        to the input angle
        
        Parameters
        ----------
        angle : float
            The angle used to compute the point coordinates on the circle.
            It is considered to be in radians
        radius : float
            The distance from the circle centre
        
        Returns
        -------
        numpy.array
            Array with the point coordinates
            
        """        
        x = self.centre[0] + radius*np.cos(angle)
        y = self.centre[1] + radius*np.sin(angle)
        return np.array([x, y])        
        
    def point(self, angle):
        """
        Function used to calculate a point coordinates, belonging to
        the circle, given the input angle
        
        Parameters
        ----------
        angle : float
            The angle used to compute the point coordinates on the circle.
            It is considered to be in radians
            
        Returns
        -------
        numpy.array
            Array with the point coordinates
            
        """
        return self.calcPoint(angle, self.radius)

    def expandPoint(self, angle, expFac):
        """
        Function used to calculate a point coordinates, from the circle centre,
        with a different radius. The distance is computed as the circle radius 
        times the input factor
        
        Parameters
        ----------
        angle : float
            The angle used to compute the point coordinates on the circle.
            It is considered to be in radians
        expFac : float 
            Expansion coefficient, used as `radius * expFac`, for the calculation 
            of the point
            
        Returns
        -------
        numpy.array
            Array with the point coordinates
            
        """
        try :
            expFac > 1
        except Error :
            print("Expansion coefficient: {: 1.3g} must be greater than 1!".format(expFac))

        return self.calcPoint(angle, self.radius*expFac)

